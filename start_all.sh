#!/bin/sh

RUNME=./sc/start_frame.scd

# for model B internal soundcard
# jackd -R -d alsa -r 41000 -P hw:0 -o 2 -S
# external multichannel USB
jackd -R -d alsa -d hw:1 -r 44100 -n 4 -p 512 -zs &
sleep 1
xvfb-run --auto-servernum /usr/bin/sclang $RUNME 
