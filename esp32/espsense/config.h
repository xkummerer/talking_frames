// config happens here, no need to mess
// with espsense.ino

// essid & password for the wifi AP:
const char* ssid = "ESSID";
const char* password = "pa55w0rd";

// ip and port of OSC message receiver:
const char * ip = "192.168.0.2";
const unsigned int port = 6543;

// speed control
#define UPDATE_INTERVAL_MS 100
