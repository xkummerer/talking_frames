#include "config.h"

#include <CapacitiveSensor.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>

WiFiClient tcp;
WiFiUDP udp;

CapacitiveSensor cs_4_2 = CapacitiveSensor(4,2);

void setup() {
  Serial.begin(115200);

  // connect to AP
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("\nconnecting");  
  while(WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(100);
  }
  Serial.println("\nconnected");
  
  // test 
  // tcp.connect(ip, 8000);
  // tcp.print("GET /\r\n\r\n\n");
  // tcp.stop();

  cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF); 
}

void loop() {
  long data = cs_4_2.capacitiveSensor(70);
  
  if (WiFi.status() == WL_CONNECTED) {
    OSCMessage msg("/sensor");
    msg.add((int)data);
    udp.beginPacket(ip, port);
    msg.send(udp);
    udp.endPacket();
    msg.empty();
  }
  
  delay(UPDATE_INTERVAL_MS);
}
