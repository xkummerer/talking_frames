#include <stdio.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tinyusb.h"
#include "esp_timer.h"

#include "driver/gpio.h"
#include "driver/touch_pad.h"

#include "midi.h"

#define TOUCH_PIN 6
#define BLINK_GPIO 15

uint8_t touch_read = 0;

static void touch_init(void)
{
  ESP_ERROR_CHECK(touch_pad_init());
  touch_pad_config(TOUCH_PIN);

  touch_pad_denoise_t denoise = {
    /* The bits to be cancelled are determined according to the noise level. */
    .grade = TOUCH_PAD_DENOISE_BIT4,
    .cap_level = TOUCH_PAD_DENOISE_CAP_L4,
  };
  touch_pad_denoise_set_config(&denoise);
  touch_pad_denoise_enable();

  touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
  touch_pad_fsm_start();
}

static void touch_read_task(void *pvParameter)
{
  uint16_t value;
  vTaskDelay(100 / portTICK_PERIOD_MS);
  while (1) {
    touch_pad_read_raw_data(TOUCH_PIN, &value);
    touch_read = value >> 9;
  }
  vTaskDelay(200 / portTICK_PERIOD_MS);
}

static void midi_init (void)
{
  tinyusb_config_t const tusb_cfg = {
    .device_descriptor = NULL, 
    .string_descriptor = s_str_desc,
    .external_phy = false,
    .configuration_descriptor = s_midi_cfg_desc,
  };
  ESP_ERROR_CHECK(tinyusb_driver_install(&tusb_cfg));
}

static void midi_read_task(void *arg)
{
  // The MIDI interface always creates input and output port/jack descriptors
  // regardless of these being used or not. Therefore incoming traffic should be read
  // (possibly just discarded) to avoid the sender blocking in IO
  uint8_t packet[4];
  bool read = false;
  for (;;) {
    gpio_set_level(BLINK_GPIO, false); 
    vTaskDelay(1);
    while (tud_midi_available()) {
      read = tud_midi_packet_read(packet);
      if (read) {
        gpio_set_level(BLINK_GPIO, true);
      }
    }
  }
}

// Basic MIDI Messages
#define NOTE_OFF 0x80
#define NOTE_ON  0x90
#define MIDI_CC 0xb0 // | channel 0x0num 0x0val
#define CHAN 0

static void midi_writer(void *arg)
{
  static uint8_t const cable_num = 0; // MIDI jack associated with USB endpoint

  if (tud_midi_mounted()) {
    uint8_t cc[3] = {MIDI_CC|CHAN, 64, touch_read};
    tud_midi_stream_write(cable_num, cc, 3);
    /*   uint8_t note_on[3] = {NOTE_ON|CHAN, 64, 127}; */
    /*   tud_midi_stream_write(cable_num, note_on, 3); */
    /*   uint8_t note_off[3] = {NOTE_OFF|CHAN, 64, 0}; */
    /*     tud_midi_stream_write(cable_num, note_off, 3); */
  }
}

void app_main(void)
{
  midi_init();
  touch_init();

  gpio_reset_pin(BLINK_GPIO);
  gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

  int const tempo = 250;
  const esp_timer_create_args_t midi_args = {
    .callback = &midi_writer,
    .name = "midi_writer"
  };

  esp_timer_handle_t midi_timer;
  ESP_ERROR_CHECK(esp_timer_create(&midi_args, &midi_timer));
  ESP_ERROR_CHECK(esp_timer_start_periodic(midi_timer, tempo*1000));

  xTaskCreate(midi_read_task, "midi_read_task", 2*1024, NULL, 5, NULL);
  xTaskCreate(touch_read_task, "touch_read_task", 2*1024, NULL, 5, NULL);
}
