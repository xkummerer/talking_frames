#!/usr/bin/fennel

;; run as:
;; $ fennel make-script.fnl > make-waves.sh
;; then:
;; $ mkdir nums/
;; $ sh make-waves.sh
;; then move/copy nums/ to some place supercollider can find it
;; requires fennel & flite to be installed, obviously

(each [_ v (ipairs [:one :two :three :four :five :six])]
  (print (string.format "echo %s | flite -o nums/%s.wav" v v)))

